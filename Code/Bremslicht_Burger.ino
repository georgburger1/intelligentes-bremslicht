#include<Wire.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

//----------Variablen Definieren----------------
//Led-Strip
#define PIN        6
#define PIN1       9
#define PIN2       10
#define PIN3       11
#define NUMPIXELS 15
#define DELAYVAL 0 

//Accelerometer
const int MPU=0x68; 
int16_t AcX,AcY,AcZ;
float G_wert;

//Blinker
int blinker_L = 2;
int blinker_R = 3;
int Status_L=LOW;
int Status_R=LOW;
int light_Status =0;
int front_light = 4;
int front_light_level = 0;
const int interval = 1000;
unsigned int previousMillis = 0; 

//----------------------------------------------

Adafruit_NeoPixel Links_H(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel Rechts_H(NUMPIXELS, PIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel Links_V(NUMPIXELS, PIN2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel Rechts_V(NUMPIXELS, PIN3, NEO_GRB + NEO_KHZ800);


void setup() {
//--------------------LED-Strip------------------------
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
 Links_H.begin();
 Rechts_H.begin();
 Links_V.begin();
 Rechts_V.begin();   
//-----------------------------------------------------

//-------------------Accelerometer---------------------
Wire.begin();
Wire.beginTransmission(MPU);
Wire.write(0x6B);
Wire.write(0);
Wire.endTransmission(true);
//-----------------------------------------------------

Serial.begin(9600);
pinMode(blinker_L,INPUT);
pinMode(blinker_R,INPUT);
pinMode(front_light,INPUT);
}

void loop() {
  
Wire.beginTransmission(MPU);
Wire.write(0x3B);  
Wire.endTransmission(false);
Wire.requestFrom(MPU,14,true);
    
//Acceleration data correction
int AcXoff = -950;
int AcYoff = -300;
int AcZoff = 0;

//read accel data
AcX=(Wire.read()<<8|Wire.read()) + AcXoff;
AcY=(Wire.read()<<8|Wire.read()) + AcYoff;
AcZ=(Wire.read()<<8|Wire.read()) + AcZoff;

Serial.print("X = "); Serial.print(AcX/16384.0);
Serial.print(" | Y = "); Serial.print(AcY/16384.0);

//Serial.print("Z = "); Serial.print(AcZ/16384.0);
G_wert=(AcX/16384.0);
Serial.print(" | G = "); Serial.print(G_wert);

//delay(300);
unsigned int currentMillis = millis();
Status_L=digitalRead(blinker_L);
Status_R=digitalRead(blinker_R);

  front(Status_L,Status_R);

  if((Status_L==HIGH)&&(Status_R!=HIGH)){
    if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    if(light_Status){
      blinker_links_on();  
    } 
    }
  }
  if((Status_R==HIGH)&&(Status_L!=HIGH)){
    if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    if(light_Status){
      blinker_rechts_on();  
    }
    }
  }
 if(G_wert>-0.35){
  brakelight(20);
  Serial.print("  20 ");
} 
if((G_wert<=-0.35)&&(G_wert>-0.6)){
  brakelight(80);
  Serial.print("  80 ");
  delay(300);
}
if((G_wert<=-0.6)&&(G_wert>-0.9)){
  brakelight(150);
  Serial.print("  160 ");
  delay(300);
}
if(G_wert<=-0.9){
  Serial.print("  255 ");
  hard_braking();
 
}
Serial.println(" ");

}




void brakelight(int helligkeit){
  for(int i=0; i<NUMPIXELS; i++) { 
    Links_H.setPixelColor(i, Links_H.Color(helligkeit, 0, 0));
    Rechts_H.setPixelColor(i, Rechts_H.Color(helligkeit, 0, 0));
    Links_H.show();
    Rechts_H.show();
  }
  light_Status = 1;
}

void blinker_links_on(){
  Serial.println("Blinker Links ");
  
  for(int i=0; i<NUMPIXELS; i++) { 
    Links_V.setPixelColor(i, Links_V.Color(20, 20, 20));
    Links_V.show();
    i++;
  }
    
  for(int i=0; i<NUMPIXELS; i++) { 
    if((i%2)==0){
      Links_H.setPixelColor(i, Links_H.Color(255, 255, 0));
      Links_V.setPixelColor(i, Links_V.Color(255, 255, 0));
    }
    Links_H.show();
    Links_V.show();
    delay(10);
  }
  light_Status = 0;
}

void blinker_rechts_on(){
  //Links_H.clear(); // Set all pixel colors to 'off'  
  Serial.println("Blinker Rechts ");
  for(int i=0; i<NUMPIXELS; i++) { 
    if((i%2)==0){
      Rechts_H.setPixelColor(i, Rechts_H.Color(255, 255, 0));
      Rechts_V.setPixelColor(i, Rechts_V.Color(255, 255, 0));
    }
    Rechts_H.show();
    Rechts_V.show();
    delay(10);
  }
  light_Status = 0;
}

void hard_braking(){
  for(int w=0;w<10;w++){
  for(int i=0; i<NUMPIXELS; i++) { 
    Links_H.setPixelColor(i, Links_H.Color(255, 0, 0));
    Rechts_H.setPixelColor(i, Rechts_H.Color(255, 0, 0));
    Links_H.show();
    Rechts_H.show();
    //delay(DELAYVAL);
  }
  delay(100);
  for(int i=0; i<NUMPIXELS; i++) { 
    Links_H.setPixelColor(i, Links_H.Color(0, 0, 0));
    Rechts_H.setPixelColor(i, Rechts_H.Color(0, 0, 0));
    Links_H.show();
    Rechts_H.show();
    //delay(DELAYVAL);
  }
  delay(100);
}
}

void front(int Status_L,int Status_R){
  
  int front_helligkeit_L;
  int front_helligkeit_R;

if((digitalRead(front_light)==LOW)){
  front_helligkeit_R = 20;
  front_helligkeit_L = 20;
}else{
  Serial.println("Front Licht");
  if((digitalRead(front_light)==HIGH)&&(Status_L==HIGH)){
    front_helligkeit_R = 255;
    front_helligkeit_L = 20;
  } else if((digitalRead(front_light)==HIGH)&&(Status_R==HIGH)){
    front_helligkeit_R = 20;
    front_helligkeit_L = 255;
  } else {
    front_helligkeit_L = 255;
    front_helligkeit_R = 255;
  }
}
  
  for(int i=0; i<NUMPIXELS; i++) { 
    Links_V.setPixelColor(i, Links_V.Color(front_helligkeit_L, front_helligkeit_L, front_helligkeit_L));
    Rechts_V.setPixelColor(i, Rechts_V.Color(front_helligkeit_R, front_helligkeit_R, front_helligkeit_R));
    Links_V.show();
    Rechts_V.show();
  }
}
