# Intelligentes Bremslicht

# Inhaltsverzeichnis
1. [Beschreibung](#1-beschreibung) <br>
2. [Hardware](#2-hardware) <br>
2.1 [Schaltung am Steckbrett](#21-schaltung-am-steckbrett) <br>
2.2 [Platinen Entwurf](#22-platinen-entwurf) <br>
2.3 [Position der Led-Bänder](#23-position-der-led-bänder) <br>
3. [Software](#3-software) <br>
3.1 [Code](#31-code) <br>
3.2 [Verbesserungen](#32verbesserungen) <br>
3.2.1 [Sichtbarkeit der Frontblinker](#321-sichtbarkeit-der-frontblinker) <br>
3.2.2 [Einstellung der Parameter](#322-einstellung-der-parameter) <br>
4. [Resultat](#4-resultat) <br>
5. [Betriebsanleitung](#5-betriebsanleitung) <br>
5.1 [Befestigung](#51-befestigung) <br>
5.2 [Bedienung](#52-bedienung) <br>

# 1. Beschreibung

Das Projekt ist ein Beleuchtungssystem für Fahrräder. Inbegriffen in dem Projekt ist ein Frontlicht, mit zwei Helligkeitsstufen und ein Rück- bzw. Bremslicht, welches die Helligkeit je nach Geschwindigkeit und Bremsstärke verändert. Zusätzlich gibt es eine Blinkerfunktion. 

# 2. Hardware

- Mikrocontroller: [Arduino Nano](#https://www.amazon.de/AZDelivery-Atmega328-gratis-Arduino-kompatibel/dp/B01LWSJBTD?pd_rd_w=uqTqU&pf_rd_p=f7938a98-1a74-4398-ad44-196e1c61bed1&pf_rd_r=Q7XA22ESN3VDGXFBZB5B&pd_rd_r=d22a7ff6-49fa-4dbe-af34-93a83a3a3aee&pd_rd_wg=2XPR7&ref_=sspa_dk_rhf_yoy_pt_sub_2&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzMUkyM1E3S0lYSzZIJmVuY3J5cHRlZElkPUEwNDU5NTIzM05aUEVYSDc0RVFYTSZlbmNyeXB0ZWRBZElkPUEwNDkyNTI0QjVUSldCMlBQVElOJndpZGdldE5hbWU9c3BfcmhmX3lveSZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1) mit dem ATmega328 [Datenbaltt](./Datenblätter/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf)
- Beschleunigungssensor [GY-521 mit MPU 6050](#https://www.amazon.de/ARCELI-Beschleunigungsmesser-Gyroskop-Beschleunigungssensor-Datenausgang/dp/B07BVXN2GP/ref=sr_1_7?keywords=GY521&qid=1643033715&sr=8-7) [Datenbaltt](./Datenblätter/MPU-6000-Datasheet1.pdf)
- [LED-Streifen mit WS2812b](#LEDshttps://www.amazon.de/gp/product/B01CDTEE5W/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1) [Datenblatt](./Datenblätter/WS2812B.pdf)
- Kippschalter
	- [(Ein/Aus/Ein) Schalter](#https://www.conrad.at/de/p/tru-components-tc-ta103a1-kippschalter-250-v-ac-3-a-1-x-ein-aus-ein-rastend-0-rastend-1-st-1587775.html?searchType=SearchRedirect)
	- [(Ein/Aus) Schalter](#https://www.conrad.at/de/p/apem-5537a-kippschalter-250-v-ac-3-a-1-x-ein-aus-ein-tastend-0-tastend-1-st-700351.html)
- 10k Ohm Widerstände
- [Batterie-Shield](#https://www.diymore.cc/products/18650-16340-lithium-battery-shield-v9-v8-v3-mobile-power-expansion-board-module-5v-3a-3v-micro-usb-type-c-for-arduino-esp8266)
- [18650 Li-Ion Akkus](#https://www.akkushop-austria.at/at/lg-inr18650mj1-3500mah-36v-37v-li-ion-akku-abmessungen-651x183mm-beachten-flattop)

## 2.1 Schaltung am Steckbrett

Der Testaufbau zur Testung des Codes wurde auf einem Steckbrett aufgebaut und mit [Fritzing](#https://fritzing.org/releases) visualisiert.


![Steckbrettaufbau](./Schaltplan/Fritzing_Schaltplan.png)

## 2.2 Platinen Entwurf
Für den Entwurf der Paltine wurde das Programm [KiCad](#https://www.kicad.org/download/) verwendet.

Damit die Leiterbahnen kreuzungsfrei verlaufen, wurden zwei Layer verwendet. Dadurch dass alle Bauteile bis auf den Beschleunigungssensor, Mikrocontroller und die Widerstände nicht auf der Platine sitzen, wurden ausschließlich Pin-Header verwendet. Die Pin-Header für den Sensor und Mikrocontroller wurden verwendet, damit sie ausgetauscht werden können.


![Schematic](./Schaltplan/Schaltplan_sch.png) ![PCB](./Schaltplan/Pcb.png)

Für den ersten Prototyp wurde eine Lochrasterplatine verwendet und die Verbindungen zwischen Pin-Header und den drei Widerständen mit Drähten verlötet.


![Platine Front](./Schaltplan/Platine_Front.png) ![Platine Rückseite](./Schaltplan/Platine_Back.png)

## 2.3 Position der Led-Bänder

Bei der Befestigung der Led-Bänder wurde über die Sichtbarkeit und die Wahrnehmung der Lichter und Blinker nachgedacht. 
Daher wurden die LED-Bänder an der vorderen und der hinteren Gabel befestigt.
- Dadurch ist gewährleistet, dass andere Vekehrsteilnehmer das linke und rechte Blinken eindeutig unterscheiden können. 
- Weiters bieten die Gabelen Platz für 15 Leds und dadurch für genügend Leuchtleistung


# 3. Software
- Entwicklungsumgebung: [Arduino IDE](#https://www.arduino.cc/en/software)
- Bibliotheken
	- [Neopixel](#https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library-use) von Adafruit
	- [Wire](#https://www.arduino.cc/en/reference/wire) von Arduino

- Kommunikation zwischen Beschleunigungssenor und Mikrocontroller erfolgt mittels I2C. Dafür wird die Wire.h Bibliothek und die analogen Pins A4->SDA, A5->SCL verwendet. 

- Um die WS2812b Leds ansteuern zu können wird ein PWM-Signal am Led-Streifen angelegt. Dafür wurden die digitalen PWM-fähigen Pins D6, D9, D10, D11 verwendet. 

## 3.1 Code 

### Funktionen
- void brakelight(int helligkeit)
	- Diese Funktion wird nach jeder Messung des Beschleunigungssensors aufgerufen. Je nach größe des Messwerts, wird eine Helligkeitsstufe übergeben.

- void hard_braking()
	- Die Funktion hard_braking() wird aufgerufen bei einer Vollbremsung. Die Rückleuchten blinken mit voller Helligkeit im Abstand von 100ms.

- void blinker_links_on() und void blinker_rechts_on()
	- Die Blinker-Funktionen werde aufgerufen, wenn durch die Betätigung des [Schalters für den Blinker](./Videos/Lenker.JPG) an den digitalen Pins 2 und 3 ein HIGH-Signal anliegt. Dadurch dass die delay()-Funktion nicht verwendet werden konnte, da ansonsten die Messung des Beschleunigungssenors unterbrochen wird, wurde dies mittels "Zeitstempel" und der mills()-Funktion realisiert. Dabei wird die Zeitdiffernez ermittelt und die Blinker ein- bzw. ausgeschaltet.

- void front(int Status_L,int Status_R)
	- Diese Funktion wird aufgerufen, wenn der [Schalter für das Fernlicht](./Videos/Lenker.JPG) betätigt wird und am digitalen Pin 4 ein HIGH-Signal anliegt. Die Übergabeparameter werden verwendet um sicher zu gehen, dass die Blinker während des aktivierten Fernlichts gut sichtbar sind ([siehe 3.2.1 Sichtbarkeit der Frontblinker](#321-sichtbarkeit-der-frontblinker)).


## 3.2 Verbesserungen

### 3.2.1 Sichtbarkeit der Frontblinker
Bei voller Helligkeit des Frontlichts war die Erkennbarkeit der Frontblinker vermindert. Daher wurde der Code so verändert, dass sich der LED-Streifen, der sich im Blinkermodus befindet abdunkelt und der andere auf voller Helligkeit bleibt.

### 3.2.2 Einstellung der Parameter
Um die Helligkeit abhängig von der Bremsstärke zu machen, wurde je nach Beschleunigungswertebereich die Helligkeit eingestellt.
Dafür wurde Bremsungen bei verschiedenen Geschwindigkeiten durchgeführt und die Wertebereiche angepasst.  

# 4. Resulat

![Schalter für Blinker und Fernlicht](./Videos/Lenker.JPG) 
![Forntlicht](./Videos/Frontlicht.MOV) ![Rücklicht](./Videos/Ruecklicht.mov)

![Vorführung](./Videos/Testrunde.MOV) ![Normales Bremsen](./Videos/Normales_Bremsen.MOV) ![Starkes Bremsen](./Videos/Starkes_Bremsen.MOV)

# 5. Betriebsanleitung

## 5.1 Befestigung
- Am Lenker werden zwei die [Schalter](./Videos/Lenker.JPG) für den Blinker und das Frontlicht montiert.
- Das Gehäuse wird waagrecht auf der Querstrebe des Rahmens befestigt. Die waagrechte Postion ist wichtig, damit der Beschleunigungssensor die Messdaten richtig erfasst wird.
- LED-Streifen für das Rücklicht werden auf der hinteren Gabel montiert
- LED-Streifen für Frontlicht werden entweder auf der Frontgabel oder auf dem Lenker befesigt. 

## 5.2 Bedienung

- Nach richtiger Befestigung kann die Beleuchtung mit einem einfachen Knopfdurck am Gehäuse eingeschaltet und mit doppelt Knopfdruck abgeschalten werden.
- Mit den zwei Kippschaltern kann der Blinker und das Fernlicht eingeschaltet werden.
- Zum Aufladen gibt es eine USB und USB-Typ-C Buchse.
